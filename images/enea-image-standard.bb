DESCRIPTION = "Base image for the Standard profile"

require images/enea-image-common.inc

IMAGE_ROOTFS_EXTRA_SPACE = "131072"
IMAGE_OVERHEAD_FACTOR = "2"
